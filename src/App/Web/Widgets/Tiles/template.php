<?php
/**
 * @var array $items
 */

?>
<?php if ($items): ?>
    <div class="row">
        <?php foreach ($items as $item): ?>
            <div class="col-12 col-md-6 col-xl-4 mb-4">
                <div class="card border-primary h-100">
                    <div class="card-body">
                        <h5 class="card-title">
                            <span class="badge badge-primary">#<?= $item['id'] ?></span>
                        </h5>
                        <p class="card-text"><?= $item['name'] ?></p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php else: ?>
    <div class="alert alert-light">
        Пусто
    </div>
<?php endif; ?>
