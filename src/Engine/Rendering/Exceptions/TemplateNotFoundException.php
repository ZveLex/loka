<?php

namespace Engine\Rendering\Exceptions;


use Engine\Exception;

class TemplateNotFoundException extends Exception
{

}
