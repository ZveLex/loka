<?php


namespace App\Web\Widgets\Breadcrumb;


use Engine\AbstractWidget;

class Widget extends AbstractWidget
{
    public function getView(array $items = []): string
    {
        return $this->renderer->generate(
            __DIR__ . '/template.php',
            [
                'items' => $items
            ]
        );
    }
}
