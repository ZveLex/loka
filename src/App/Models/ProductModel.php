<?php


namespace App\Models;


use Engine\AbstractModel;

class ProductModel extends AbstractModel
{
    /**
     * Возвращает все товары
     * @return array
     */
    public function getAll(): array
    {
        $stmt = $this->pdo->query("SELECT * FROM products");

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Возвращает все товары категории, рекурсивно учитывает товары вложенных групп
     * @param int $groupId
     * @return array
     */
    public function getRecursiveGroupProducts(int $groupId): array
    {
        $stmt = $this->pdo->prepare(
            "WITH RECURSIVE `chain` AS
                        (
                         SELECT `groups`.* FROM `groups` WHERE `groups`.`id`= ?
                         
                         UNION ALL 
                         
                         SELECT child.*
                         FROM `groups` child
                         INNER JOIN `chain` parent ON parent.id = child.id_parent
                        )
                         SELECT products.* 
                         FROM `chain`
                         INNER JOIN products ON products.id_group = `chain`.id
                         ORDER BY products.id
                         "
        );
        $stmt->execute([$groupId]);

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}
