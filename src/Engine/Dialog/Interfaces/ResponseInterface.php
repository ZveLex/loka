<?php

namespace Engine\Dialog\Interfaces;

interface ResponseInterface
{
    public function __construct(
        string $content,
        int $code,
        array $headers
    );

    /**
     * Отправляет заголовки и содержимое ответа клиенту
     */
    public function send(): void;
}
