<?php
/**
 * @var array $items
 * @var string $message
 */

?>
<div class="list-group">
    <?php if ($items): ?>
        <?php foreach ($items as ['href' => $href, 'title' => $title, 'count' => $count, 'items' => $child]): ?>
            <a href="<?= $href ?>"
               class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                <?= $title ?>
                <?php if ($count): ?>
                    <span class="badge badge-primary badge-pill"><?= $count ?></span>
                <?php endif; ?>
            </a>
            <?php if ($child): ?>
                <?php foreach ($child as ['href' => $href, 'title' => $title, 'count' => $count]): ?>
                    <a href="<?= $href ?>"
                       class="list-group-item list-group-item-action list-group-item-secondary text-muted d-flex justify-content-between align-items-center pl-5">
                        <?= $title ?>
                        <?php if ($count): ?>
                            <span class="badge badge-light badge-pill"><?= $count ?></span>
                        <?php endif; ?>
                    </a>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="alert alert-light mb-0">
            Пусто
        </div>
    <?php endif; ?>
</div>
