<?php

namespace Engine\Dialog\Interfaces;

interface RequestInterface
{
    public function __construct(
        string $method,
        string $uriPath,
        string $content,
        array $queryParameters,
        array $requestParameters,
        array $headers,
        array $cookies
    );

    /**
     * Возвращает метод запроса
     * @return string
     */
    public function getMethod(): string;

    /**
     * Возвращает Uri путь
     * @return string
     */
    public function getUriPath(): string;

    /**
     * Возвращает тело запроса
     * @return string
     */
    public function getContent(): string;

    /**
     * Возвращает GET параметр
     * @param string $name
     * @return string|null
     */
    public function getQueryParameter(string $name): ?string;

    /**
     * Возвращает POST параметры
     * @param string $name
     * @return string|null
     */
    public function getRequestParameter(string $name): ?string;

    /**
     * Возвращает заголовок по его наименованию
     * @param string $name
     * @return string|null
     */
    public function getHeader(string $name): ?string;

    /**
     * Возвращает Cookie по его наименованию
     * @param string $name
     * @return string|null
     */
    public function getCookie(string $name): ?string;

    /**
     * Возвращает все GET параметры
     * @return array
     */
    public function getAllQueryParameters(): array;

    /**
     * Возвращает все POST параметры
     * @return array
     */
    public function getAllRequestParameters(): array;

    /**
     * Возвращает все заголовки
     * @return array
     */
    public function getAllHeaders(): array;

    /**
     * Возвращает все cookie
     * @return array
     */
    public function getAllCookies(): array;
}
