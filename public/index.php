<?php

use Engine\Core;
use Engine\Dialog\RequestFactory;

require dirname(__DIR__) . '/vendor/autoload.php';

define('DIR_ROOT', dirname(__DIR__));

(new Core(
    DIR_ROOT . '/.env',
    [
        \App\ServiceProvider::class
    ]
))
    ->handle(RequestFactory::createFromGlobals())
    ->send();
