<?php

use Engine\Rendering\Interfaces\RendererInterface;
use Engine\Rendering\Renderer;
use Engine\Routing\Interfaces\RouterInterface;
use Engine\Routing\Router;

return [
    RendererInterface::class => new Renderer(),
    RouterInterface::class => new Router(),
    PDO::class => static function () {
        return new PDO(
            "mysql:dbname={$_ENV['MYSQL_DATABASE']};host=mysql;port={$_ENV['MYSQL_PORT']}",
            $_ENV['MYSQL_USER'],
            $_ENV['MYSQL_PASSWORD']
        );
    }
];
