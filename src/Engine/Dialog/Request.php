<?php

namespace Engine\Dialog;


use Engine\Dialog\Interfaces\RequestInterface;

class Request implements RequestInterface
{
    public const METHOD_GET = 'GET';
    public const METHOD_POST = 'POST';

    protected string $method;
    protected string $uriPath;
    protected string $content;
    protected array $queryParameters;
    protected array $requestParameters;
    protected array $headers;
    protected array $cookies;

    public function __construct(
        string $method,
        string $uriPath,
        string $content = '',
        array $queryParameters = [],
        array $requestParameters = [],
        array $headers = [],
        array $cookies = []
    ) {
        $this->method = $method;
        $this->uriPath = $uriPath !== '' ? $uriPath : '/';
        $this->content = $content;
        $this->queryParameters = $queryParameters;
        $this->requestParameters = $requestParameters;
        $this->headers = $headers;
        $this->cookies = $cookies;
    }

    /**
     * @inheritDoc
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @inheritDoc
     */
    public function getUriPath(): string
    {
        return $this->uriPath;
    }

    /**
     * @inheritDoc
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @inheritDoc
     */
    public function getQueryParameter(string $name): ?string
    {
        return $this->getItemFromArrayProperty('queryParameters', $name);
    }

    /**
     * @inheritDoc
     */
    public function getRequestParameter(string $name): ?string
    {
        return $this->getItemFromArrayProperty('requestParameters', $name);
    }

    /**
     * @inheritDoc
     */
    public function getHeader(string $name): ?string
    {
        return $this->getItemFromArrayProperty('headers', $name);
    }

    /**
     * @inheritDoc
     */
    public function getCookie(string $name): ?string
    {
        return $this->getItemFromArrayProperty('cookies', $name);
    }

    /**
     * @inheritDoc
     */
    public function getAllQueryParameters(): array
    {
        return $this->queryParameters;
    }

    /**
     * @inheritDoc
     */
    public function getAllRequestParameters(): array
    {
        return $this->requestParameters;
    }

    /**
     * @inheritDoc
     */
    public function getAllHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @inheritDoc
     */
    public function getAllCookies(): array
    {
        return $this->cookies;
    }

    /**
     * Вспомогательный метод для уменьшения дублирования кода
     * @param string $propertyName
     * @param string $key
     * @return string|null
     */
    private function getItemFromArrayProperty(string $propertyName, string $key): ?string
    {
        $property = $this->$propertyName;

        return array_key_exists($key, $property) ? $property[$key] : null;
    }
}
