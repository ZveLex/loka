<?php


namespace Engine\Env;


use Engine\Env\Exceptions\EnvFileNotFoundExceptions;

class FileLoader
{
    /**
     * Загружает переменные окружения из файла .env в глобальный массив $_ENV
     * @param string $file
     * @throws EnvFileNotFoundExceptions
     */
    public function load(string $file): void
    {
        if (!is_file($file)) {
            throw new EnvFileNotFoundExceptions("Не найден файл '{$file}'");
        }

        $lines = explode(PHP_EOL, file_get_contents($file));

        if ($lines) {
            foreach ($lines as $line) {
                [$name, $value] = explode('=', $line);
                $_ENV[trim($name)] = trim($value);
            }
        }
    }
}
