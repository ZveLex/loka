<?php

namespace Engine\Utils;


use Engine\Exceptions\InvalidArgumentException;
use Engine\Exceptions\RuntimeException;

class FileSystemUtil
{
    /**
     * Нормализует абсолютный путь,
     * заменяет слэши на корректные для текущей операционной системы,
     * обрабатывает относительные переходы "." и "..",
     * удаляет двойные слеши
     *
     * @param string $path
     *
     * @return string
     */
    public static function normalizePath(string $path): string
    {
        $search = DIRECTORY_SEPARATOR === '/' ? ['\\\\', '\\.\\', '\\'] : ['//', '/./', '/'];

        $path = str_replace($search, DIRECTORY_SEPARATOR, $path);

        if (strpos($path, '..') !== false) {
            $segments = [];
            foreach (explode(DIRECTORY_SEPARATOR, $path) as $item) {
                if ($item === '..') {
                    array_pop($segments);
                } else {
                    $segments[] = $item;
                }
            }
            if (count($segments) === 1) {
                array_unshift($segments, '');
            }
            $resultPath = implode(DIRECTORY_SEPARATOR, $segments);
        } else {
            $resultPath = $path;
        }

        return $resultPath;
    }

    /**
     * Рекурсивное создание всех папок пути, по указанному пути должны быть права на запись
     *
     * @param string $path абсолютный путь
     * @param string $mode с какими unix правами создавать папки, строка с тремя цифрами '755', '775' и т.д.
     * @param bool $throw указывает нужно ли выкидывать исключение, если не удалось создать папку
     *
     * @return string возвращает нормализованный путь созданной папки
     * @throws RuntimeException
     * @throws InvalidArgumentException
     */
    public static function makeDir(string $path, string $mode = '777', bool $throw = true): string
    {
        $path = self::normalizePath($path);

        if (is_dir($path)) {
            return $path;
        }

        if (strlen($mode) === 3) {
            $mode = intval('0' . $mode, 8);
        } else {
            throw new InvalidArgumentException('Unix права задаются строкой с тремя цифрами');
        }

        if (!mkdir($path, $mode, true) && !is_dir($path) && $throw) {
            throw new RuntimeException("Не удалось создать директорию {$path}");
        }

        return $path;
    }
}
