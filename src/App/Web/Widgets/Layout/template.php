<?php
/**
 * @var string $header шапка
 * @var string $breadcrumb хлебные крошки
 * @var string $sidebar боковая панель
 * @var string $title заголовок
 * @var string $content содержимое
 */

?>
<?= $header ?>
<div class="container-fluid">
    <h3 class="my-4"><?= $title ?></h3>
    <?= $breadcrumb ?>
    <div class="row">
        <aside class="col-12 col-sm-6 col-md-5 col-lg-4 col-xl-3 mb-3">
            <?= $sidebar ?>
        </aside>
        <main class="col-12 col-sm-6 col-md-7 col-lg-8 col-xl-9">
            <?= $content ?>
        </main>
    </div>
</div>

