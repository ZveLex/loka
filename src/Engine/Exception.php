<?php


namespace Engine;


class Exception extends \Exception
{
    protected int $httpCode = 500;
    protected string $publicMessage = '500 Внутренняя ошибка в работе сервиса';

    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    /**
     * @return string
     */
    public function getPublicMessage(): string
    {
        return $this->publicMessage;
    }
}
