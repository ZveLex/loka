<?php

namespace Engine\Rendering\Interfaces;


use Engine\Rendering\Exceptions\TemplateNotFoundException;

interface RendererInterface
{
    /**
     * Генерация представления
     * @param string $path путь до шаблона - абсолютный или относительный от папки $_ENV['DIR_COMMON_TEMPLATES']
     * @param array $args
     * @return string
     * @throws TemplateNotFoundException
     */
    public function generate(string $path, array $args = []): string;
}
