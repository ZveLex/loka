<?php
/**
 * @var array $items
 */

?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <?php if ($items): ?>
            <?php foreach ($items as ['href' => $href, 'title' => $title]): ?>
                <li class="breadcrumb-item"><a href="<?= $href ?>"><?= $title ?></a></li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ol>
</nav>
