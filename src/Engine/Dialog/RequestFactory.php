<?php

namespace Engine\Dialog;


use Engine\Dialog\Interfaces\RequestInterface;

class RequestFactory
{
    /**
     * Создает запрос из глобальных переменных
     * @return Request
     */
    public static function createFromGlobals(): RequestInterface
    {
        return new Request(
            $_SERVER['REQUEST_METHOD'],
            parse_url($_SERVER['REQUEST_URI'])['path'],
            file_get_contents('php://input'),
            $_GET,
            $_POST,
            getallheaders(),
            $_COOKIE,
        );
    }
}
