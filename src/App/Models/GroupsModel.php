<?php


namespace App\Models;


use Engine\AbstractModel;

class GroupsModel extends AbstractModel
{
    /**
     * Возвращает все дочерние группы
     * @param int $parentId
     * @return array
     */
    public function getChildGroups(int $parentId): array
    {
        $stmt = $this->pdo->prepare("SELECT * FROM `groups` WHERE id_parent = ?");
        $stmt->execute([$parentId]);

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Возвращает группу по id
     * @param int $id
     * @return array
     */
    public function getGroup(int $id): array
    {
        $stmt = $this->pdo->prepare("SELECT * FROM `groups` WHERE id = ?");
        $stmt->execute([$id]);

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Возвращает количество товаров в группе, также рекурсивно учитывает все товары в подгруппах
     * @param int $groupId
     * @return int
     */
    public function getRecursiveCountProducts(int $groupId): int
    {
        $stmt = $this->pdo->prepare(
            "WITH RECURSIVE `chain` AS
                        (
                         SELECT `groups`.* FROM `groups` WHERE `groups`.`id`= ?
                         
                         UNION ALL 
                         
                         SELECT child.*
                         FROM `groups` child
                         INNER JOIN `chain` parent ON parent.id = child.id_parent
                        )
                         SELECT COUNT(products.id) `count`
                         FROM `chain`
                         INNER JOIN products ON products.id_group = `chain`.id"
        );
        $stmt->execute([$groupId]);

        return $stmt->fetch(\PDO::FETCH_ASSOC)['count'];
    }

    /**
     * Возвращает цепочку родительских групп
     * @param int $groupId
     * @return array
     */
    public function getParentChain(int $groupId): array
    {
        $stmt = $this->pdo->prepare(
            "WITH RECURSIVE `chain` AS
                        (
                         SELECT `groups`.* FROM `groups` WHERE `groups`.`id`= ?
                         
                         UNION ALL 
                         
                         SELECT parent.*
                         FROM `groups` parent
                         INNER JOIN `chain` child ON child.id_parent = parent.id
                        )
                         SELECT `chain`.*
                         FROM `chain`"
        );
        $stmt->execute([$groupId]);

        return array_reverse($stmt->fetchAll(\PDO::FETCH_ASSOC));
    }
}
