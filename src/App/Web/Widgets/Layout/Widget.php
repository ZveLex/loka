<?php


namespace App\Web\Widgets\Layout;


use Engine\AbstractWidget;
use Engine\Rendering\Interfaces\RendererInterface;

class Widget extends AbstractWidget
{
    private \App\Web\Widgets\Header\Widget $header;
    private \App\Web\Widgets\Sidebar\Widget $sidebar;
    private \App\Web\Widgets\Breadcrumb\Widget $breadcrumb;

    public function __construct(
        RendererInterface $renderer,
        \App\Web\Widgets\Header\Widget $header,
        \App\Web\Widgets\Sidebar\Widget $sidebar,
        \App\Web\Widgets\Breadcrumb\Widget $breadcrumb
    ) {
        parent::__construct($renderer);
        $this->header = $header;
        $this->sidebar = $sidebar;
        $this->breadcrumb = $breadcrumb;
    }

    public function getView(
        string $title,
        string $content,
        array $sidebarItems,
        array $breadcrumbItems
    ): string {
        $content = $this->renderer->generate(
            __DIR__ . '/template.php',
            [
                'header' => $this->header->getView(),
                'breadcrumb' => $this->breadcrumb->getView($breadcrumbItems),
                'sidebar' => $this->sidebar->getView($sidebarItems),
                'title' => $title,
                'content' => $content,
            ]
        );

        return $this->renderer->generate(
            'html.php',
            [
                'content' => $content
            ]
        );
    }
}
