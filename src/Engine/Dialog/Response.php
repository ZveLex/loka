<?php

namespace Engine\Dialog;


use Engine\Dialog\Interfaces\ResponseInterface;

class Response implements ResponseInterface
{
    public const CODE_OK = 200;
    public const CODE_NOT_FOUND = 404;
    public const CODE_INTERNAL_ERROR = 500;

    private array $codeHeaders = [
        self::CODE_OK => 'HTTP/1.1 200 ОK',
        self::CODE_NOT_FOUND => 'HTTP/1.1 404 Not Found',
        self::CODE_INTERNAL_ERROR => 'HTTP/1.1 500 Internal Error',
    ];

    protected string $content;
    protected array $headers;
    protected int $code;

    public function __construct(
        string $content,
        int $code = self::CODE_OK,
        array $headers = []
    ) {
        $this->content = $content;
        $this->code = $code;
        $this->headers = $headers;
    }

    /**
     * @inheritDoc
     */
    public function send(): void
    {
        if (array_key_exists($this->code, $this->codeHeaders)) {
            array_unshift($this->headers, $this->codeHeaders[$this->code]);
        }

        $this->headers = array_unique($this->headers);

        foreach ($this->headers as $header) {
            header($header);
        }

        echo $this->content;
    }
}
