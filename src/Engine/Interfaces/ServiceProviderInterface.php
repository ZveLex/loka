<?php


namespace Engine\Interfaces;


interface ServiceProviderInterface
{
    /**
     * Запускается при инициализации сервиса
     */
    public function init(): void;
}
