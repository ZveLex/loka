<?php

namespace Engine;

use Engine\DependencyInjection\Container;
use Engine\DependencyInjection\Exceptions\ContainerException;
use Engine\DependencyInjection\Exceptions\ContainerNotFoundException;
use Engine\Dialog\Interfaces\RequestInterface;
use Engine\Dialog\Interfaces\ResponseInterface;
use Engine\Dialog\Response;
use Engine\Env\FileLoader;
use Engine\Env\Exceptions\EnvFileNotFoundExceptions;
use Engine\Exceptions\RuntimeException;
use Engine\Exceptions\InvalidArgumentException;
use Engine\Interfaces\ServiceProviderInterface;
use Engine\Routing\Interfaces\RouterInterface;
use Engine\Utils\FileSystemUtil;

class Core
{
    private Container $container;

    /**
     * @param string $pathToEnvFile Путь до файла .env
     * @param array $serviceProviderClasses массив наименований сервис провайдеров
     * @param array $containerItems элементы контейнера
     * @throws ContainerException
     * @throws ContainerNotFoundException
     * @throws EnvFileNotFoundExceptions
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function __construct(
        string $pathToEnvFile,
        array $serviceProviderClasses,
        $containerItems = []
    ) {
        if (!defined('DIR_ROOT')) {
            throw new RuntimeException(
                'Не определена константа DIR_ROOT, в которой должен храниться путь до корня проекта'
            );
        }

        // Загрузка файла .env

        (new FileLoader())->load($pathToEnvFile);

        //Создание контейнера

        $items = require __DIR__ . '/default_container_items.php';
        $items[__CLASS__] = $this;

        $this->container = new Container(array_merge($containerItems, $items));

        $this->initErrorHandling();

        // Инициализация сервис провайдеров

        if (!$serviceProviderClasses) {
            throw new RuntimeException('Массив наименований сервис провайдеров не может быть пустым');
        }

        foreach ($serviceProviderClasses as $serviceProviderClass) {
            $this->initServiceProvider($this->container->get($serviceProviderClass));
        }
    }

    /**
     * Обрабатывает запрос
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function handle(RequestInterface $request): ResponseInterface
    {
        try {
            $handler = $this->container->get(RouterInterface::class)->getHandler($request);
            $response = $this->invokeHandler($handler, $request);
            if (is_scalar($response)) {
                $response = new Response($response);
            }
        } catch (\Throwable $ex) {
            $response = $this->getErrorResponse($request, $ex);
        }

        return $response;
    }

    /**
     * Запускает обработчик запроса и возвращает результат его работы
     * @param $handler
     * @param RequestInterface $request
     * @return mixed
     * @throws ContainerException
     * @throws ContainerNotFoundException
     * @throws RuntimeException
     */
    private function invokeHandler($handler, RequestInterface $request)
    {
        if (is_string($handler)) {
            if (function_exists($handler)) {
                $response = $handler($request);
            } elseif (class_exists($handler)) {
                $object = $this->container->get($handler);
                $response = $object($request);
            } else {
                throw new RuntimeException("Обработчик '{$handler}' не найден");
            }
        } elseif (is_array($handler)) {
            [$class, $method] = $handler;

            $object = $this->container->get($class);

            $response = $object->$method($request);
        } elseif ($handler instanceof \Closure) {
            $response = $handler->bindTo(null)->__invoke($request);
        } else {
            throw new RuntimeException('Не поддерживаемый тип обработчика запроса');
        }

        return $response;
    }

    /**
     * Логирует ошибку и возвращает экземпляр ответа ошибки
     * @param RequestInterface $request
     * @param \Throwable $ex
     * @return Response
     */
    private function getErrorResponse(
        RequestInterface $request,
        \Throwable $ex
    ): ResponseInterface {
        $logMessage = sprintf(
            '%s %s' . PHP_EOL . '%s' . PHP_EOL . '%s' . PHP_EOL . '%s',
            $request->getMethod(),
            $request->getUriPath(),
            $ex->getMessage(),
            $ex->getTraceAsString(),
            str_pad('', 50, '-')
        );
        error_log(sprintf('%s ' . PHP_EOL . '%s', $uriPath, $logMessage));

        $code = 500;
        $message = 'Произошла непредвиденная ошибка в работе сервиса';
        if ($ex instanceof Exception) {
            $code = $ex->getHttpCode();
            $message = $ex->getPublicMessage();
        }

        return new Response($message, $code);
    }

    /**
     * Инициализация обработки ошибок
     * @throws Exceptions\InvalidArgumentException
     * @throws Exceptions\RuntimeException
     */
    private function initErrorHandling(): void
    {
        // Регистрация обработчика фатальных ошибок

        register_shutdown_function(
            static function () {
                $error = error_get_last();
                if (isset($error) &&
                    !in_array(
                        $error['type'],
                        [
                            E_NOTICE,
                            E_DEPRECATED,
                            E_WARNING
                        ],
                        true
                    )) {
                    (bool)ob_get_status() && ob_end_clean();

                    header('HTTP 1.1 500 Internal Error');
                    exit('Произошла непредвиденная ошибка в работе сервиса');
                }
            }
        );

        // Выключение вывода ошибок

        ini_set('display_errors', 'off');
        ini_set('display_startup_errors ', 'off');

        // Включение и настройка логирования

        ini_set('log_errors', 'on');
        ini_set('log_errors_max_len ', 0);

        //Создание папки для логов
        $dirLogs = FileSystemUtil::makeDir(DIR_ROOT . "/{$_ENV['DIR_LOGS']}");

        //Файл для логов

        $fileLogs = $dirLogs . DIRECTORY_SEPARATOR . date('Ymd') . '.log';
        ini_set('error_log', $fileLogs);
    }

    /**
     * Вспомогательный метод для проверки интерфейса
     * @param ServiceProviderInterface $serviceProvider
     */
    private function initServiceProvider(ServiceProviderInterface $serviceProvider): void
    {
        $serviceProvider->init();
    }
}
