FROM php:7.4-fpm

ADD https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions /usr/local/bin/

WORKDIR /app

COPY ./composer.json ./composer.lock /app/

RUN chmod uga+x /usr/local/bin/install-php-extensions \
    && sync \
    && install-php-extensions \
    pdo_mysql \
    && apt-get update -y \
    && apt-get install -y \
    git \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/local/bin/composer \
    && rm -rf /usr/local/bin/install-php-extensions \
    && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["bash", "-c", "composer install && php-fpm"]
