<?php


namespace Engine;


use Engine\Rendering\Interfaces\RendererInterface;

abstract class AbstractWidget
{
    /**
     * @var RendererInterface
     */
    protected RendererInterface $renderer;

    public function __construct(RendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }
}
