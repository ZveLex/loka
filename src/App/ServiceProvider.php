<?php


namespace App;


use App\Web\Handlers\MainHandler;
use Engine\Dialog\Request;
use Engine\Interfaces\ServiceProviderInterface;
use Engine\Routing\Interfaces\RouterInterface;

class ServiceProvider implements ServiceProviderInterface
{
    private RouterInterface $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function init(): void
    {
        $this->router->addRoute(Request::METHOD_GET, '/', MainHandler::class);
    }
}
