<?php

namespace Engine\Routing;

use Engine\Dialog\Interfaces\RequestInterface;
use Engine\Routing\Exceptions\MatchNotFoundException;
use Engine\Routing\Interfaces\RouterInterface;

class Router implements RouterInterface
{
    private array $routes = [];

    /**
     * @inheritDoc
     */
    public function addRoute(string $method, string $uriPath, $handler): RouterInterface
    {
        if (!array_key_exists($method, $this->routes)) {
            $this->routes[$method] = [];
        }

        $this->routes[$method][$uriPath] = $handler;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getHandler(RequestInterface $request)
    {
        $method = $request->getMethod();
        $uriPath = $request->getUriPath();

        if (!array_key_exists($method, $this->routes) || !array_key_exists($uriPath, $this->routes[$method])) {
            throw new MatchNotFoundException("Обработчик запроса '{$method} {$uriPath}' не найден");
        }

        return $this->routes[$method][$uriPath];
    }
}
