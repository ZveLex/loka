<?php


namespace App\Web\Handlers;


use App\Models\GroupsModel;
use App\Models\ProductModel;
use Engine\Dialog\Interfaces\RequestInterface;

class MainHandler
{
    private \App\Web\Widgets\Layout\Widget $layoutWidget;
    private \App\Web\Widgets\Tiles\Widget $tilesWidget;
    private GroupsModel $groupsModel;
    private ProductModel $productModel;

    public function __construct(
        \App\Web\Widgets\Layout\Widget $layoutWidget,
        \App\Web\Widgets\Tiles\Widget $tilesWidget,
        GroupsModel $groupsModel,
        ProductModel $productModel
    ) {
        $this->layoutWidget = $layoutWidget;
        $this->tilesWidget = $tilesWidget;
        $this->groupsModel = $groupsModel;
        $this->productModel = $productModel;
    }

    public function __invoke(RequestInterface $request)
    {
        $queryGroupId = (int)$request->getQueryParameter('group');

        $sidebarItems = [];
        $breadcrumbItems = $this->getBreadcrumbsItems($queryGroupId);


        if ($queryGroupId) {
            $currentGroup = $this->groupsModel->getGroup($queryGroupId);
            $sidebarItems[] = [
                'href' => "/?group={$currentGroup['id_parent']}",
                'title' => '..',
                'count' => false
            ];

            $title = $currentGroup['name'];
            $products = $this->productModel->getRecursiveGroupProducts($queryGroupId);
        } else {
            $title = 'Все товары';
            $products = $this->productModel->getAll();
        }

        $sidebarItems = array_merge($sidebarItems, $this->getSidebarItems($queryGroupId));

        return $this->layoutWidget->getView(
            $title,
            $this->tilesWidget->getView($products),
            $sidebarItems,
            $breadcrumbItems
        );
    }

    private function getSidebarItems(int $queryGroupId, bool $recursive = true): array
    {
        $sidebarItems = [];
        $groups = $this->groupsModel->getChildGroups($queryGroupId);
        if ($groups) {
            foreach ($groups as ['id' => $id, 'name' => $name]) {
                $sidebarItem = [
                    'href' => "/?group={$id}",
                    'title' => $name,
                    'count' => $this->groupsModel->getRecursiveCountProducts($id),
                ];
                if ($recursive) {
                    $sidebarItem['items'] = $this->getSidebarItems($id, false);
                }
                $sidebarItems[] = $sidebarItem;
            }
        }

        return $sidebarItems;
    }

    private function getBreadcrumbsItems(int $queryGroupId): array
    {
        $breadcrumbItems = [
            [
                'href' => '/',
                'title' => 'Начало',
            ]
        ];
        if ($queryGroupId) {
            foreach ($this->groupsModel->getParentChain($queryGroupId) as ['id' => $id, 'name' => $name]) {
                $breadcrumbItems[] = [
                    'href' => '/?group=' . $id,
                    'title' => $name,
                ];
            }
        }

        return $breadcrumbItems;
    }
}
