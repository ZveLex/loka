<?php

namespace Engine\Routing\Exceptions;

use Engine\Exception;

class MatchNotFoundException extends Exception
{
    protected int $httpCode = 404;
    protected string $publicMessage = '404 Ресурс не найден';
}
