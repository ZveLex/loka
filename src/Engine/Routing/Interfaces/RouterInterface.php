<?php

namespace Engine\Routing\Interfaces;


use Engine\Dialog\Interfaces\RequestInterface;
use Engine\Routing\Exceptions\MatchNotFoundException;

interface RouterInterface
{
    /**
     * Регистрирует обработчик маршрута
     * @param string $method
     * @param string $uriPath
     * @param $handler
     * @return RouterInterface
     */
    public function addRoute(string $method, string $uriPath, $handler): RouterInterface;

    /**
     * Возвращает обработчик запроса
     * @param RequestInterface $request
     * @return mixed
     * @throws MatchNotFoundException
     */
    public function getHandler(RequestInterface $request);
}
