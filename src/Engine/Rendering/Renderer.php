<?php

namespace Engine\Rendering;


use Engine\Rendering\Exceptions\TemplateNotFoundException;
use Engine\Rendering\Interfaces\RendererInterface;
use Engine\Utils\FileSystemUtil;


class Renderer implements RendererInterface
{
    private string $dirCommonTemplates;

    public function __construct()
    {
        $this->dirCommonTemplates = DIR_ROOT . '/' . $_ENV['DIR_COMMON_TEMPLATES'];
    }

    /**
     * @inheritDoc
     */
    public function generate(string $path, array $args = []): string
    {
        $path = FileSystemUtil::normalizePath($path);
        if (!is_file($path)) {
            $path = FileSystemUtil::normalizePath($this->dirCommonTemplates . '/' . $path);
        }
        if (!is_file($path)) {
            throw new TemplateNotFoundException("Не удалось найти файл шаблона {$path}");
        }

        extract($args);
        unset($args);
        ob_start();
        include $path;

        return ob_get_clean();
    }
}
