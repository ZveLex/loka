<?php
/**
 * @var string $content содержимое html документа
 */
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>App</title>
    <link rel="stylesheet" href="/libs/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
<?= $content ?>
<script src="/libs/jquery/3.5.1/jquery.slim.min.js"></script>
<script src="/libs/popper/1.16.1/popper.min.js"></script>
<script src="/libs/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
