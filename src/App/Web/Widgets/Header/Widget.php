<?php


namespace App\Web\Widgets\Header;


use Engine\AbstractWidget;

class Widget extends AbstractWidget
{
    public function getView(): string
    {
        return $this->renderer->generate(__DIR__ . '/template.php');
    }
}
