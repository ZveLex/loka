<?php

namespace Engine\DependencyInjection;


use Engine\DependencyInjection\Exceptions\ContainerException;
use Engine\DependencyInjection\Exceptions\ContainerNotFoundException;

class Container
{
    private array $items;

    public function __construct(array $items = [])
    {
        $items[__CLASS__] = $this;

        $this->items = $items;
    }

    /**
     * Находит элемент в контейнере по его идентификатору и возвращает его.
     * Если элемента нет, а идентификатор является наименованием существующего класса,
     * то произойдет попытка автоматического создания экземпляра этого класса.
     *
     * @param string $id Идентификатор элемента
     * @return mixed
     * @throws ContainerNotFoundException
     * @throws ContainerException
     */
    public function get($id)
    {
        if ($this->has($id)) {
            // Для отложенной инициализации экземпляра используется анонимная функция как фабрика
            if ($this->items[$id] instanceof \Closure) {
                $this->items[$id] = $this->items[$id]->bindTo(null)->__invoke($this);
            }

            return $this->items[$id];
        }

        if (class_exists($id)) {
            // Автоматическое разрешение зависимостей по типу параметра в конструкторе

            try {
                $reflection = new \ReflectionClass($id);
                $constructor = $reflection->getConstructor();
                if ($constructor === null) {
                    $item = new $id();
                } else {
                    $parameters = $constructor->getParameters();

                    if (empty($parameters)) {
                        $item = new $id();
                    } else {
                        $args = [];
                        foreach ($parameters as $parameter) {
                            $parameterClassName = $parameter->getClass()->getName();
                            $args[] = $this->get($parameterClassName);
                        }
                        $item = new $id(...$args);
                    }
                }

                $this->items[$id] = $item;

                return $item;
            } catch (\ReflectionException $ex) {
                throw new ContainerException(
                    "Ошибка при автоматическом создании элемента в контейнере. {$ex->getMessage()}"
                );
            }
        }

        throw new ContainerNotFoundException("Элемент '{$id}' не найден в контейнере.");
    }

    /**
     * Возвращает true, если в контейнере есть элемент с таким идентификатором.
     * Иначе возвращает false
     *
     * @param string $id Идентификатор элемента
     * @return bool
     */
    public function has($id): bool
    {
        return array_key_exists($id, $this->items);
    }
}
